from acp_times import open_time, close_time
 
def test_open_time1():   
    assert open_time(60, 60, "2017-01-01 00:00") == "2017-01-01T01:46:00.00+00:00" 

def test_open_time2():
    assert open_time(120, 120, "2017-01-01 00:00") == "2017-01-01T03:32:00.00+00:00"

def test_open_time3():
    assert open_time(175, 175, "2017-01-01 00:00") != "2017-01-01T06:00:00.00+00:00"

def test_close_time1():
    assert close_time(60, 60, "2017-01-01 00:00") == "2017-01-01T04:00:00.00+00:00"

def test_close_time2():
    assert close_time(120, 120, "2017-01-01 00:00") != "2017-01-01T08:30:00.00+00:00"
